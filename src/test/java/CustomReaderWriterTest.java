import org.junit.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;

import java.io.FileWriter;
import java.io.IOException;

import static org.junit.Assert.*;

public class CustomReaderWriterTest {
    private final String fin = "fin.txt";
    private final String fout = "fout.txt";
    private final String encodingIn = "utf8";
    private final String encodingOut = "KOI8_R";
    private final String text = "Съешь ещё этих мягких французских булок, да выпей же чаю.\n" +
            "The quick brown fox jumps over the lazy dog.\n";

    @ParameterizedTest
    @ValueSource(strings = {text, "АБВГД\n"})
    public void customReaderParam(String textIn) throws IOException {
        fullFile(fin, textIn);
        CustomReaderWriterImp customReaderWriterImp = new CustomReaderWriterImp(fin, fout, encodingIn, encodingOut);
        assertEquals(customReaderWriterImp.readFromFileEncoding(), textIn);
        customReaderWriterImp.init(fin, fout, encodingIn, encodingOut);
        assertEquals(customReaderWriterImp.readFromFileEncoding(), textIn);
    }

    @ParameterizedTest
    @ValueSource(strings = {text, "АБВГД\n"})
    public void customWriter(String textIn) throws IOException {
        fullFile(fin, textIn);
        CustomReaderWriterImp customReaderWriterImp = new CustomReaderWriterImp(fin, fout, encodingIn, encodingOut);
        customReaderWriterImp.writeToFileEncoding(customReaderWriterImp.readFromFileEncoding());
        customReaderWriterImp.init("fout.txt", "fin.txt", "KOI8_R", "utf8");
        assertEquals(customReaderWriterImp.readFromFileEncoding(), textIn);
    }
    
    private void fullFile(String fileName, String textFile) {
        try (FileWriter writer = new FileWriter(fileName, false)) {
            writer.write(textFile);
            writer.flush();
        } catch (IOException ex) {
            System.out.println(ex.getMessage());
        }
    }

}
