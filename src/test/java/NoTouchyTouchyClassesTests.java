import com.google.common.collect.ImmutableMap;
import org.junit.Test;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;

import static org.junit.Assert.assertEquals;

public class NoTouchyTouchyClassesTests {
    @Test
    public void testNoChangesToReferenceClasses() throws Exception {
        final ImmutableMap<String, String> src2Ref = ImmutableMap.<String, String>builder()
                .put("src/main/java/CustomReaderWriter.java", "src/test/resources/ref/CustomReaderWriter.java")
                .build();

        src2Ref.forEach((src, ref) -> assertSourceEqualsReference(Paths.get(src), Paths.get(ref)));
    }

    private void assertSourceEqualsReference(final Path src, final Path ref) {
        try {
            final List<String> refLines = Files.readAllLines(src);
            final List<String> srcLines = Files.readAllLines(ref);
            assertEquals(src + " was modified", refLines, srcLines);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }
}
