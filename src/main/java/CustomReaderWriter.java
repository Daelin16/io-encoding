import java.io.IOException;

public interface CustomReaderWriter {
    String readFromFileEncoding() throws IOException;

    void writeToFileEncoding(String fileText) throws IOException;
}
