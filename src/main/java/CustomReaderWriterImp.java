import java.io.*;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;

public class CustomReaderWriterImp implements CustomReaderWriter {
    private BufferedReader bufferedReader;
    private BufferedWriter bufferedWriter;
    public FileInputStream is;
    public FileOutputStream os;
    public OutputStreamWriter osr;
    public InputStreamReader isr;

    public CustomReaderWriterImp(String fin, String fout, String encodingIn, String encodingOut) {
        init(fin, fout, encodingIn, encodingOut);
    }

    @Override
    public String readFromFileEncoding() throws IOException {
        String str, fileText = "";
        while ((str = bufferedReader.readLine()) != null) {
            fileText += str + "\n";
        }
        bufferedReader.close();
        return fileText;
    }

    @Override
    public void writeToFileEncoding(String fileText) throws IOException {
        bufferedWriter.append(fileText);
        bufferedWriter.close();
    }

    public void init(String fin, String fout, String encodingIn, String encodingOut) {
        try {
            is = new FileInputStream(fin);
            isr = new InputStreamReader(is, encodingIn);
            bufferedReader = new BufferedReader(isr);
            os = new FileOutputStream(fout);
            osr = new OutputStreamWriter(os, encodingOut);
            bufferedWriter = new BufferedWriter(osr);
        } catch (FileNotFoundException | UnsupportedEncodingException e) {
            e.printStackTrace();
        }

    }

}
