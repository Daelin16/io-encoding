import java.io.IOException;

public interface CustomReaderWriter {
    String readFromFileEncoding() throws IOException;

    String writeToFileEncoding(String fileText) throws IOException;
}
